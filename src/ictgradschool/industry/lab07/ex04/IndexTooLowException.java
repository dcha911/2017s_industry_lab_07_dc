package ictgradschool.industry.lab07.ex04;

public class IndexTooLowException extends Exception {
    public IndexTooLowException() {
    }

    public IndexTooLowException(String message) {
        super(message);
    }

    public IndexTooLowException(String message, Throwable cause) {
        super(message, cause);
    }

    public IndexTooLowException(Throwable cause) {
        super(cause);
    }
}
