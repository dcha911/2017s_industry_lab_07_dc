package ictgradschool.industry.lab07.ex04;

public class IndexTooHighException extends Exception {
    public IndexTooHighException() {
    }

    public IndexTooHighException(String message) {
        super(message);
    }

    public IndexTooHighException(String message, Throwable cause) {
        super(message, cause);
    }

    public IndexTooHighException(Throwable cause) {
        super(cause);
    }
}
