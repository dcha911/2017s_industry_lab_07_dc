package ictgradschool.industry.lab07.ex02;

import ictgradschool.Keyboard;

import java.security.Key;

/**
 * A guessing game!
 */
public class GuessingGame {

    /**
     * Plays the actual guessing game.
     *
     * You shouldn't need to edit this method for this exercise.
     */
    public void start() {

        int number = getRandomValue();
        int guess = 0;

        while (guess != number) {

            guess = getUserGuess();

            if (guess > number) {
                System.out.println("Too high!");
            }
            else if (guess < number) {
                System.out.println("Too low!");
            }
            else {
                System.out.println("Perfect!");
            }

        }

    }

    /**
     * Gets a random integer between 1 and 100.
     *
     * You shouldn't need to edit this method for this exercise.
     */
    private int getRandomValue() {
        return (int) (Math.random() * 100) + 1;
    }

    /**
     * Gets the user's guess from the keyboard. Currently assumes that the user will always enter a valid guess.
     *
     * TODO Implement some error handling, for the cases where the user enters a value that's too big, too small, or
     * TODO not an integer. Change this method so it's guaranteed to return an integer between 1 & 100, inclusive.
     */
//    private int getUserGuess() {
//        int num = 0;
//        boolean success = false;
//        while (!success) {
//
//            try {
//                System.out.println("enter a number between 1 & 100");
//                num = Integer.parseInt(Keyboard.readInput());
//                success=true;
//            } catch (NumberFormatException e) {
//                System.out.println("thats not a number");
//            }
//        }
//    return num;

//        System.out.println("enter your guess");
//        int guess = Integer.parseInt(Keyboard.readInput());
//        while (guess < 1 || guess > 100) {
//            try {
//                System.out.println("your guess must be between 1 & 100");
//                System.out.println("enter your guess");
//                guess = Integer.parseInt(Keyboard.readInput());
//
//            } catch (NumberFormatException e) {
//                System.out.println("user error, try again");
//                System.out.println("enter your guess");
//                guess = Integer.parseInt(Keyboard.readInput());
//            }
//            return guess;
//        }
//        return guess;
//    }

//        int check = 1;
//        int guess = 0;
//        do {
//            try {
//                System.out.println("enter your guess");
//                guess = Integer.parseInt(Keyboard.readInput());
//                while (guess<1 || guess>100) {
//                    System.out.println("your guess must be between 1 and 100");
//                    System.out.println("enter a proper guess, dick");
//                    guess = Integer.parseInt(Keyboard.readInput());
//                }
//                check = 2;
//            } catch (Exception e) {
//                System.out.println("user error, your guess should be a number");
//            }
//            }while (check==1);
//           return guess;
//        }

    private int getUserGuess() {
        int userGuess = 0;
        boolean correctGuess = false;

        while (correctGuess == false) {
            try {
                System.out.println("enter a guess between 1 & 100");
                userGuess = (Integer.parseInt(Keyboard.readInput()));
                if (userGuess >= 1 && userGuess <= 100) {
                    correctGuess = true;
                } else System.out.println("your number is out of bounds");
            } catch (NumberFormatException e) {
                System.out.println("that's not a real number");
            }
        }

        return userGuess;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
